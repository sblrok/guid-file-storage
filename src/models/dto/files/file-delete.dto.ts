import { IsNotEmpty, IsString, IsUUID } from 'class-validator';

export class FileDeleteDTO {
    @IsNotEmpty()
    @IsUUID()
    public guid: string;

    @IsNotEmpty()
    @IsString()
    public storage: string;
}
