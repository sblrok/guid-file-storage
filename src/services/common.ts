import { BadRequestError } from 'routing-controllers';

export function checkPathname(name: string) {
    // TODO: добавить более строгие правила для директории
    if (name.includes('/') || name.includes('\\')) {
        throw new BadRequestError('Incorrect pathname');
    }
}
