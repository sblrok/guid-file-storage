import {
    Delete, Get, JsonController,
    OnUndefined, Param,
    Post, UploadedFile,
} from 'routing-controllers';
import { validateDTO } from './common';
import { FileUpsertDTO } from '../models/dto/files/file-upsert.dto';
import { FileListDTO } from '../models/dto/files/file-list.dto';
import { FilesService } from '../services/files.service';
import { FileGetDTO } from '../models/dto/files/file-get.dto';
import { FileDeleteDTO } from '../models/dto/files/file-delete.dto';

@JsonController('/files')
export class FilesController {
    constructor(private fileService: FilesService) {}

    @Get('/:storage/list')
    public async list(
        @Param('storage') storage: string,
    ) {
        const listDTO = new FileListDTO();
        listDTO.storage = storage;

        await validateDTO(listDTO);

        return this.fileService.list(listDTO);
    }

    @Get('/:storage/:guid')
    public async get(
        @Param('storage') storage: string,
        @Param('guid') guid: string,
    ) {
        const getDTO = new FileGetDTO();
        getDTO.guid = guid;
        getDTO.storage = storage;

        await validateDTO(getDTO);

        return this.fileService.get(getDTO);
    }

    @Post('/:storage/:guid')
    @OnUndefined(204)
    public async upsert(
        @Param('storage') storage: string,
        @Param('guid') guid: string,
        @UploadedFile('file') file: any,
    ) {
        const upsertDTO = new FileUpsertDTO();
        upsertDTO.guid = guid;
        upsertDTO.storage = storage;
        upsertDTO.file = file;

        await validateDTO(upsertDTO);

        return this.fileService.upsert(upsertDTO);
    }

    @Delete('/:storage/:guid')
    @OnUndefined(204)
    public async delete(
        @Param('storage') storage: string,
        @Param('guid') guid: string,
    ) {
        const deleteDTO = new FileDeleteDTO();
        deleteDTO.guid = guid;
        deleteDTO.storage = storage;

        await validateDTO(deleteDTO);

        await this.fileService.delete(deleteDTO);
    }
}
