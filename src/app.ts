import * as express from 'express';
import { createExpressServer, useContainer as useRouteContainer } from 'routing-controllers';
import { useContainer as useTypeormContainer } from 'typeorm';
import { useContainer  as useValidatorContainer } from 'class-validator';
import { Container } from 'typedi';
import { config } from './config';
import { logger } from './logger';
import { ErrorHandlerMiddleware, SetHeadersMiddleware } from '@middlewares';
import { FilesController } from './controllers/files.controller';
import * as fs from 'fs';

export class App {
    public app: express.Application;

    constructor() {
        useRouteContainer(Container);
        useTypeormContainer(Container);
        useValidatorContainer(Container);

        this.app = createExpressServer({
            routePrefix: '',
            defaultErrorHandler: false,
            classTransformer: true,
            validation: false,
            controllers: [
                FilesController,
            ],
            middlewares: [
                SetHeadersMiddleware, // TODO: check it for production
                ErrorHandlerMiddleware,
            ],
        });

        this.app.listen(config.port, () => {
            if (!fs.existsSync(config.rootDir)) {
                fs.mkdirSync(config.rootDir);
            }
            fs.accessSync(config.rootDir);

            logger.info(`Listening at http://${config.host}:${config.port}/`);
        });
    }
}

export const app = new App().app;
