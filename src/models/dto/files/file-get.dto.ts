import { IsNotEmpty, IsString, IsUUID } from 'class-validator';

export class FileGetDTO {
    @IsNotEmpty()
    @IsUUID()
    public guid: string;

    @IsNotEmpty()
    @IsString()
    public storage: string;
}
