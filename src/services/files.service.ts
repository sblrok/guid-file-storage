import { Service } from 'typedi';
import { NotFoundError } from 'routing-controllers';
import { FileGetDTO } from '../models/dto/files/file-get.dto';
import * as fs from 'fs';
import { checkPathname } from './common';
import { config } from '../config';
import { FileListDTO } from '../models/dto/files/file-list.dto';
import { FileUpsertDTO } from '../models/dto/files/file-upsert.dto';
import { FileDeleteDTO } from '../models/dto/files/file-delete.dto';

@Service()
export class FilesService {
    public async list(options: FileListDTO) {
        checkPathname(options.storage);

        const path = `${config.rootDir}/${options.storage}`;

        if (!fs.existsSync(path)) {
            throw new NotFoundError('Storage doesnt exists');
        }

        return fs.readdirSync(path);
    }

    public async get(options: FileGetDTO) {
        checkPathname(options.storage);

        const path = `${config.rootDir}/${options.storage}/${options.guid}`;

        if (!fs.existsSync(path)) {
            throw new NotFoundError('File doesnt exists');
        }

        return fs.readFileSync(path);
    }

    public async upsert(options: FileUpsertDTO) {
        checkPathname(options.storage);

        const dir = `${config.rootDir}/${options.storage}`;
        const path = `${dir}/${options.guid}`;

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }

        fs.writeFileSync(path, options.file.buffer);
    }

    public async delete(options: FileDeleteDTO) {
        checkPathname(options.storage);

        const path = `${config.rootDir}/${options.storage}/${options.guid}`;

        if (!fs.existsSync(path)) {
            throw new NotFoundError('File doesnt exists');
        }

        fs.unlinkSync(path);
    }
}
