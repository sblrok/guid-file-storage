# guid-file-storage

### The simplest microservice for storing files

## Install

1) Copy .env.example to .env
2) Fill .env file
3) Run ```npm ci```
4) Run ```npm run start```

## API

---
**GET /files/:storage/:guid**

*Returns the file if it exists, otherwise 404*

---
**GET /files/:storage/list**  

*Returns a list of file names in :storage*  

---
**POST /files/:storage/:guid**  
Body: file

*Creates or replaces a file with :storage as the storage name and :guid as the file name*

---
**DELETE /files/:storage/:guid**  

*Deletes file*

