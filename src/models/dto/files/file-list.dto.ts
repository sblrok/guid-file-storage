import { IsNotEmpty, IsString } from 'class-validator';

export class FileListDTO {
    @IsNotEmpty()
    @IsString()
    public storage: string;
}
