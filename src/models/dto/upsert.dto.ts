import { BadRequestError } from 'routing-controllers';

export class UpsertDTO {
    public check(keys: Array<keyof this>) {
        for (const key of keys) {
            if (this[key] === undefined) {
                throw new BadRequestError(key + ' is required');
            }
        }

        return this as Required<this>;
    }
}
