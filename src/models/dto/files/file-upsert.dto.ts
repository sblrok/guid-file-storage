import { UpsertDTO } from '../upsert.dto';
import { IsNotEmpty, IsObject, IsString, IsUUID } from 'class-validator';

export class FileUpsertDTO extends UpsertDTO {
    @IsNotEmpty()
    @IsUUID('4')
    public guid: string;

    @IsNotEmpty()
    @IsString()
    public storage: string;

    @IsNotEmpty()
    @IsObject()
    public file: any;
}
