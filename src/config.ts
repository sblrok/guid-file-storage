import convict = require('convict');
import * as dotenv from 'dotenv';

dotenv.config();

const values = convict({
    env: {
        doc: 'The application environment.',
        format: ['production', 'development', 'test'],
        default: 'development',
        env: 'NODE_ENV',
        arg: 'node-env',
    },
    host: {
        default: 'localhost',
        env: 'APP_HOST',
        doc: 'Application host',
        format: String,
    },
    port: {
        default: 9000,
        env: 'APP_PORT',
        doc: 'Application port',
        format: Number,
    },
    logLevel: {
        default: 'info',
        env: 'LOG_LEVEL',
        doc: 'Log level',
        format: String,
    },
    rootDir: {
        default: '.storage',
        env: 'ROOT_DIR',
        doc: 'Files directory',
        format: String,
    },
});

values.validate({ allowed: 'strict' });

export const config = values.getProperties();
